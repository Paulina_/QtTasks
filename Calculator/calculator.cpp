#include "calculator.h"
#include "ui_calculator.h"
#include <qlcdnumber.h>
#include <QGridLayout>
#include "qpushbutton.h"
#include <math.h>

using namespace std;

Calculator::Calculator(QWidget* pwgt/*= 0*/) : QWidget(pwgt) {
    m_plcd = new QLCDNumber(12);
    m_plcd->setSegmentStyle(QLCDNumber::Flat);
    m_plcd->setMinimumSize(150, 50);
    QChar aButtons[4][4] = {{'7', '8', '9', '/'},
                            {'4', '5', '6', '*'},
                            {'1', '2', '3', '-'},
                            {'0', '.', '=', '+'}};
    //Layout setup
    QGridLayout* ptopLayout = new QGridLayout;
    ptopLayout->addWidget(m_plcd, 0, 0, 1, 3);

    ptopLayout->addWidget(createButton("CE"), 0, 3);

    ptopLayout->addWidget(createButton("sin"), 1, 0);
    ptopLayout->addWidget(createButton("cos"), 1, 1);
    ptopLayout->addWidget(createButton("tg"), 1, 2);
    ptopLayout->addWidget(createButton("|/x"), 1, 3);

    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            ptopLayout->addWidget(createButton(aButtons[i][j]), i + 2, j);
        }
    }
    setLayout(ptopLayout);
}


QPushButton* Calculator::createButton(const QString& str)
{
    QPushButton* pcmd = new QPushButton(str);
    pcmd->setMinimumSize(40, 40);
    connect(pcmd, SIGNAL(clicked()), SLOT(slotButtonClicked()));
    return pcmd;
}


void Calculator::calculate() {
    double dOperand2 = 0;
    double dOperand1 = 0;
    double dResult = 0;
    QString strOperation = 0;

    if( m_stk.size() == 3) {
        dOperand2 = m_stk.pop().toDouble();
        strOperation = m_stk.pop();
        dOperand1 = m_stk.pop().toDouble();       

        if (strOperation == "+") {
            dResult = dOperand1 + dOperand2;
        }
        if (strOperation == "-") {
            dResult = dOperand1 - dOperand2;
        }
        if (strOperation == "/") {
            dResult = dOperand1 / dOperand2;
        }
        if (strOperation == "*") {
            dResult = dOperand1 * dOperand2;
        }
    }
    else if(m_stk.size() == 2){
        strOperation = m_stk.pop();
        dOperand1 = m_stk.pop().toDouble();

        if (strOperation == "sin") {
            dResult = sin(dOperand1*M_PI/180);
        }
        if (strOperation == "cos") {
            dResult = cos(dOperand1*M_PI/180);
        }
        if (strOperation == "tg") {
            dResult = tan(dOperand1*M_PI/180);
        }
        if (strOperation == "|/x") {
            dResult = 1/dOperand1;
        }
    }

    m_plcd->display(dResult);
}


void Calculator::slotButtonClicked() {
    bool label = false;
    QString str = ((QPushButton*)sender())->text();

    if (str == "CE") {
        m_stk.clear();
        m_strDisplay = "";
        m_plcd->display("0");
        return;
    }

    if (str.contains(QRegExp("[0-9]"))) {
        m_strDisplay += str;
        m_plcd->display(m_strDisplay.toDouble());
    }

    else if (str == ".") {
        m_strDisplay += str;
        m_plcd->display(m_strDisplay);
    }
    else{
        if (m_stk.count() == 2) {
            m_stk.push(QString().setNum(m_plcd->value()));
            calculate();
            m_stk.clear();

            if (str != "=") {
                m_stk.push(str);
            }
            m_strDisplay = "";
        }

        else if((str == "=" || str == "+" || str == "-" || str == "/" || str == "*") && m_stk.count() >= 1) {
            m_plcd->display("Error");
            m_stk.clear();
            label = true;
            m_strDisplay = "";
        }

        else if(str == "sin" || str == "cos" || str == "tg" || str == "|/x") {

            if (!m_stk.empty()) {
                m_stk.push(QString().setNum(m_plcd->value()));
                calculate();
            }

            m_stk.push(QString().setNum(m_plcd->value()));
            m_stk.push(str);

            calculate();

            m_stk.clear();
        }

        else{
            if(!label) {
                m_stk.push(QString().setNum(m_plcd->value()));
                m_stk.push(str);
                m_strDisplay = "";
            }
        }

    }
}
